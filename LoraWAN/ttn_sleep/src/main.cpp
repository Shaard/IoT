
#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "esp_sleep.h"
#include "TheThingsNetwork.h"
#include "esp_pm.h"
#include "esp_wifi.h"
#include "esp_log.h"


#define TAG_LORA "LoRa"
#define TAG_PROGRAM "Program"

// AppEUI (sometimes called JoinEUI)
const char *appEui = "xxxxx";
// DevEUI
const char *devEui = "xxxxx";
// AppKey
const char *appKey = "xxxx";

// Pins and other resources
#define TTN_SPI_HOST      SPI2_HOST
#define TTN_SPI_DMA_CHAN  SPI_DMA_DISABLED
#define TTN_PIN_SPI_SCLK  5
#define TTN_PIN_SPI_MOSI  27
#define TTN_PIN_SPI_MISO  19
#define TTN_PIN_NSS       18
#define TTN_PIN_RXTX      TTN_NOT_CONNECTED
#define TTN_PIN_RST       14
#define TTN_PIN_DIO0      26
#define TTN_PIN_DIO1      35

static TheThingsNetwork ttn;

const unsigned TX_INTERVAL = 30;

//set Pins 
void init_GPIO(){
    esp_err_t err = ESP_OK;
    gpio_config_t trig_pin = {};
    gpio_config_t echo_pin = {};

    trig_pin.intr_type = GPIO_INTR_DISABLE;
    echo_pin.intr_type = GPIO_INTR_DISABLE;

    trig_pin.mode = GPIO_MODE_OUTPUT;
    echo_pin.mode = GPIO_MODE_INPUT;

    trig_pin.pin_bit_mask = GPIO_SEL_23;
    echo_pin.pin_bit_mask = GPIO_SEL_17;

    err = gpio_config(&trig_pin);
    if (err != ESP_OK){ ESP_LOGI("failed","TrigPin"); }
    err = gpio_config(&echo_pin);
    if (err != ESP_OK){ ESP_LOGI("failed","EchoPin"); }
    gpio_set_level(GPIO_NUM_23, 1);
}

void ultrasonic(void *pvParameters){
    for (;;){
        gpio_set_level(GPIO_NUM_23, 0);
        while(gpio_get_level(GPIO_NUM_17) == 0);
        uint64_t start_time = esp_timer_get_time();
        while(gpio_get_level(GPIO_NUM_17) == 1);
        uint64_t end_time = esp_timer_get_time();
        uint64_t diff = end_time - start_time;
        //Umrechnung in cm
        static const double speed_of_sound_air = 34.3 / 1000.0; // cm/µs
        double dist = (speed_of_sound_air * diff) / 2.0;
        gpio_set_level(GPIO_NUM_23, 1);
        ESP_LOGI("Entfernung", "%.2fcm",dist);
        //Wait for Sensor Data
        vTaskDelay(100);

        if (dist > 100.00)
        {
        static uint8_t msgData[] = "Frei";
        ESP_LOGI(TAG_LORA, "Sending message Frei");
        TTNResponseCode res = ttn.transmitMessage(msgData, sizeof(msgData) - 1);
            if(res == kTTNSuccessfulTransmission){
                ESP_LOGI("Message", "Send");
            }
            else{
                ESP_LOGI("Message", "not Send");
            }
        }
        else
        {
        static uint8_t msgData[] = "Belegt";
        ESP_LOGI(TAG_LORA, "Sending message Belegt");
        TTNResponseCode res = ttn.transmitMessage(msgData, sizeof(msgData) - 1);
        if(res == kTTNSuccessfulTransmission){
                ESP_LOGI("Message", "Send");
            }
            else{
                ESP_LOGI("Message", "not Send");
            }
        }
        //wait for Data from TTN for DeepSleep
        ttn_wait_for_idle();
        ttn_prepare_for_deep_sleep();

        // Schedule wake up 30 Sec
        esp_sleep_enable_timer_wakeup(TX_INTERVAL * 1000000LL);
        ESP_LOGI("Lets Sleep", "1");
        esp_deep_sleep_start();
    }
}

extern "C" void app_main(void)
{
    ESP_LOGI(TAG_PROGRAM, "Setup Finished");
    init_GPIO();
    //Disable Wifi
    esp_wifi_set_mode(WIFI_MODE_NULL);
    //set CPU Freq to mac 80
    esp_pm_config_esp32_t pm_config = {
    .max_freq_mhz = 80,
    .min_freq_mhz= RTC_CPU_FREQ_XTAL
    };

    esp_err_t err;
    // Initialize the GPIO ISR handler service
    err = gpio_install_isr_service(ESP_INTR_FLAG_IRAM);
    ESP_ERROR_CHECK(err);
    
    // Initialize the NVS (non-volatile storage) for saving and restoring the keys
    err = nvs_flash_init();
    ESP_ERROR_CHECK(err);

    // Initialize SPI bus
    spi_bus_config_t spi_bus_config;
    spi_bus_config.miso_io_num = TTN_PIN_SPI_MISO;
    spi_bus_config.mosi_io_num = TTN_PIN_SPI_MOSI;
    spi_bus_config.sclk_io_num = TTN_PIN_SPI_SCLK;
    spi_bus_config.quadwp_io_num = -1;
    spi_bus_config.quadhd_io_num = -1;
    spi_bus_config.max_transfer_sz = 0;
    spi_bus_config.intr_flags = 0;
    err = spi_bus_initialize(TTN_SPI_HOST, &spi_bus_config, TTN_SPI_DMA_CHAN);
    ESP_ERROR_CHECK(err);

    // Configure the SX127x pins
    ttn.configurePins(TTN_SPI_HOST, TTN_PIN_NSS, TTN_PIN_RXTX, TTN_PIN_RST, TTN_PIN_DIO0, TTN_PIN_DIO1);

    // The below line can be commented after the first run as the data is saved in NVS
    ttn.provision(devEui, appEui, appKey);

    
    //After DeepSleep Always send Message
    if (ttn_resume_after_deep_sleep()) {
       ESP_LOGI("Return", "DeepSleep1");
        xTaskCreate(ultrasonic, "ultrasonic", configMINIMAL_STACK_SIZE * 3, NULL, 5, NULL);
    } else {
    ESP_LOGI("Joining...", "..");
        if (ttn.join())
            {
                ESP_LOGI("Joined", "Yes");
                xTaskCreate(ultrasonic, "ultrasonic", configMINIMAL_STACK_SIZE * 3, NULL, 5, NULL);
                
            }
            else
            {
                ESP_LOGI("Joined failed", "Goodbye");
                return;
            }
    }
}
