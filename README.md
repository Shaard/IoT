# IoT
## Dies ist ein Projekt für das Wahlpflichtfach IoT-Anwendungen HTW Saar
Bei diesem Projekt geht es um Implementierung von Parksensoren mit dem ESP32.
Schwerpunkt des Projekts liegt auf Energieverbauch des ESP32. Im Bezug auf folgende Protokolle :
* Lora
* LoraWAN
* WLAN

Anbei die Abschlusspräsentation, Projektdokumentation und ein Video

## Framework
Das Projekt wurde mit dem Plugin PlatformIO  für VS Code umgesetzt.
Für die Programmierung der ESPs wurde "ESP-IDF" verwendet.

## Backend
Im Ordner backend-main befindet sich das dazu gehörige Backend, mit Anleitung.