#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "driver/gpio.h"
#include "esp_sleep.h"


#include "esp_bt.h"

#include "esp_log.h"
#include "mqtt_client.h"

const unsigned TX_INTERVAL = 30;

static const char *TAG = "MQTT";
#define  EXAMPLE_ESP_WIFI_SSID "xxxxx"
#define  EXAMPLE_ESP_WIFI_PASS "xxxxx"
 
uint32_t MQTT_CONNEECTED = 0;

static void mqtt_app_start(void);

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id)
    {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        ESP_LOGI(TAG, "Trying to connect with Wi-Fi\n");
        break;

    case SYSTEM_EVENT_STA_CONNECTED:
        ESP_LOGI(TAG, "Wi-Fi connected\n");
        break;

    case SYSTEM_EVENT_STA_GOT_IP:
        ESP_LOGI(TAG, "got ip: startibg MQTT Client\n");
        mqtt_app_start();
        break;

    case SYSTEM_EVENT_STA_DISCONNECTED:
        ESP_LOGI(TAG, "disconnected: Retrying Wi-Fi\n");
        esp_wifi_connect();
        break;

    default:
        break;
    }
    return ESP_OK;
}

void wifi_init(void)
{
    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
         .threshold.authmode = WIFI_AUTH_WPA2_PSK,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start());
}

/*
 * @brief Event handler registered to receive MQTT events
 *
 *  This function is called by the MQTT client event loop.
 *
 * @param handler_args user data registered to the event.
 * @param base Event base for the handler(always MQTT Base in this example).
 * @param event_id The id for the received event.
 * @param event_data The data for the event, esp_mqtt_event_handle_t.
 */
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
        MQTT_CONNEECTED=1;
        
        msg_id = esp_mqtt_client_subscribe(client, "v3/iothtw@ttn/devices/eui-iot/join", 0);
        ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
        MQTT_CONNEECTED=0;
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG, "MQTT_EVENT_DATA");
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        break;
    default:
        ESP_LOGI(TAG, "Other event id:%d", event->event_id);
        break;
    }
}

esp_mqtt_client_handle_t client = NULL;
static void mqtt_app_start(void)
{
    ESP_LOGI(TAG, "STARTING MQTT1");
    ESP_LOGI(TAG, "STARTING MQTT1");
    esp_mqtt_client_config_t mqttConfig = {
        .username = "xxx",
        .password = "xxxxx",
        .uri = "mqtt://xxx:1883"};
    
    client = esp_mqtt_client_init(&mqttConfig);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);
}

void init_GPIO(){
    esp_err_t err = ESP_OK;
    gpio_config_t trig_pin = {};
    gpio_config_t echo_pin = {};

    trig_pin.intr_type = GPIO_INTR_DISABLE;
    echo_pin.intr_type = GPIO_INTR_DISABLE;

    trig_pin.mode = GPIO_MODE_OUTPUT;
    echo_pin.mode = GPIO_MODE_INPUT;

    trig_pin.pin_bit_mask = GPIO_SEL_23;
    echo_pin.pin_bit_mask = GPIO_SEL_17;

    err = gpio_config(&trig_pin);
    if (err != ESP_OK){ ESP_LOGI("failed","TrigPin"); }
    err = gpio_config(&echo_pin);
    if (err != ESP_OK){ ESP_LOGI("failed","EchoPin"); }
    gpio_set_level(GPIO_NUM_23, 1);
}

void ultrasonic(void *pvParameters)
{

 while(true)
  {
    if(MQTT_CONNEECTED){
        gpio_set_level(GPIO_NUM_23, 0);
        while(gpio_get_level(GPIO_NUM_17) == 0);
        uint64_t start_time = esp_timer_get_time();
        while(gpio_get_level(GPIO_NUM_17) == 1);
        uint64_t end_time = esp_timer_get_time();
        uint64_t diff = end_time - start_time;
        //Umrechnung in cm
        static const double speed_of_sound_air = 34.3 / 1000.0; // cm/µs
        double dist = (speed_of_sound_air * diff) / 2.0;
        gpio_set_level(GPIO_NUM_23, 1);
        //wait for Sensor Data
        vTaskDelay(100);

        if (dist > 100.00)
        {
            //Example Teste by Local MQTT, This Example not working
            esp_mqtt_client_publish(client, "v3/iothtw@ttn/devices/eui-iot/join", "Frei", 0, 0, 0);
            ESP_LOGI(TAG, "SEND DATA SUCCESFUL = Frei");
        }
        else
        {
             //Example Teste by Local MQTT, This Example not working
            esp_mqtt_client_publish(client, "v3/iothtw@ttn/devices/eui-iot/join", "Belegt", 0, 0, 0);
            ESP_LOGI(TAG, "SEND DATA SUCCESFUL = Belegt");
        }
        
        esp_deep_sleep(TX_INTERVAL * 1000000LL);
    }
  }
  
}

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    init_GPIO();
    ESP_ERROR_CHECK(ret);
    //Disable BLT
    esp_bt_controller_disable();
    wifi_init();
    xTaskCreate(ultrasonic, "ultrasonic", configMINIMAL_STACK_SIZE * 3, NULL, 5, NULL);
}

