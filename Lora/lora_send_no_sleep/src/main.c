#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "lora.h"
#include "esp_log.h"
#include "driver/gpio.h"

double dist;

//set Pins
void init_GPIO(){
   esp_err_t err = ESP_OK;
    gpio_config_t trig_pin = {};
    gpio_config_t echo_pin = {};

    trig_pin.intr_type = GPIO_INTR_DISABLE;
    echo_pin.intr_type = GPIO_INTR_DISABLE;

    trig_pin.mode = GPIO_MODE_OUTPUT;
    echo_pin.mode = GPIO_MODE_INPUT;

    trig_pin.pin_bit_mask = GPIO_SEL_23;
    echo_pin.pin_bit_mask = GPIO_SEL_17;

    err = gpio_config(&trig_pin);
    if (err != ESP_OK){ ESP_LOGI("failed","TrigPin"); }
    err = gpio_config(&echo_pin);
    if (err != ESP_OK){ ESP_LOGI("failed","EchoPin"); }
    gpio_set_level(GPIO_NUM_23, 1);
}

void ultrasonic(void *pvParameters){
  
      for(;;){
        gpio_set_level(GPIO_NUM_23, 0);
        while(gpio_get_level(GPIO_NUM_17) == 0);
        uint64_t start_time = esp_timer_get_time();
        while(gpio_get_level(GPIO_NUM_17) == 1);
        uint64_t end_time = esp_timer_get_time();
        uint64_t diff = end_time - start_time;
        //Umrechnung in cm
        static const double speed_of_sound_air = 34.3 / 1000.0; // cm/µs
        dist = (speed_of_sound_air * diff) / 2.0;
        gpio_set_level(GPIO_NUM_23, 1);
        //wait for Sensor Data
        vTaskDelay(100);

        ESP_LOGI("Start", "Main");
      if(dist > 100.00){
         lora_send_packet((uint8_t*)"frei", 4);
         ESP_LOGI("Frei2", "Main");
      }
      else{
         lora_send_packet((uint8_t*)"Belegt", 6);
         ESP_LOGI("Belegt", "Main");
      }
      ESP_LOGI("packet", "send");
      vTaskDelay(30000/ portTICK_PERIOD_MS);
      }
    
}

void app_main()
{ 
    init_GPIO();
    lora_init();
    lora_set_frequency(8681e5);
    lora_enable_crc();
    lora_set_spreading_factor(7);
    lora_set_bandwidth(125E3);
    lora_set_coding_rate(5);
    lora_set_preamble_length(8);
    lora_set_sync_word(0x12);
    xTaskCreate(ultrasonic, "ultrasonic", configMINIMAL_STACK_SIZE * 3, NULL, 5, NULL);
}

